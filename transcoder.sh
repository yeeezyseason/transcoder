#!/bin/bash

## CONFIG
	Version="0.2.1" # Version number
	inputstream="RTMP OR HLS" ## RTMP is prefered

	#HD Total Stream Bitrate = 3332kbit/s (3.25mbit/s)
	hd_output="rtmp://ADDRESSHERE"

	hd_aacrate="256k"	#	AAC Audio Bitrate	|	256kbit
	hd_minrate="1024k"	#	H264 Min Bitrate	|	1MBIT
	hd_maxrate="3072k"	#	H264 Max Bitrate	|	3MBIT
	#hd_framerate="50" N/A but included anyway


	#SD Total Stream Bitrate = 1632kbit/s (1.6mbit/s) 
	sd_output="rtmp://ADDRESSHERE"

	sd_aacrate="96k"		#	AAC Audio Bitrate	|	96kbit
	sd_minrate="750k"		#	H264 Min Bitrate	|	750kbit
	sd_maxrate="1536k"		#	H264 Max Bitrate	|	1.5MBIT
	sd_framerate="25" 		#	Framerate of 25 because of bitrate constraints
	sd_res="1024x576"		#	Downsizing to 576p because of bitrate contraints
# END CONFIG

#	Start HD Stream

screen -dmS HD \
ffmpeg -re -i $inputstream \
	-c:a libfdk_aac -b:a $hd_aacrate \
	-c:v libx264 \
	-b:v $hd_minrate -maxrate $hd_maxrate -bufsize 3074k \
	-f flv -bsf:a aac_adtstoasc $hd_output

#	Start SD Stream
screen -dmS SD \
ffmpeg -re -i $inputstream \
	-c:a libfdk_aac -b:a $sd_aacrate \
	-c:v libx264 -s $sd_res -r $sd_framerate \
	-b:v  $sd_minrate -maxrate $sd_maxrate -bufsize 1536k \
	-f flv -bsf:a aac_adtstoasc $sd_output

echo "To Check status of streams please run screen -r SD and screen -r HD"
echo "------------------------"
screen -ls
echo "------------------------"